from django.contrib import admin

from .models import Profile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'bio', 'date_of_birth']
    search_fields = ['user__username']


admin.site.register(Profile, ProfileAdmin)
