import traceback

from django.contrib.auth import user_logged_in, authenticate

from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from rest_framework.decorators import action

from .serializers import ProfileSerializer
from .models import Profile

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    http_method_names = ['post', 'get']

    @action(methods=['post'], detail=False)
    def log_in(self, request):  # TODO: rework this
        try:
            username = request.data.get('username', '')
            password = request.data.get('password', '')

            user = authenticate(username=username, password=password)
            if user is not None:
                try:
                    payload = jwt_payload_handler(user)
                    token = jwt_encode_handler(payload)
                    user_details = dict(name=user.get_full_name(),
                                        token=token)
                    user_logged_in.send(sender=user.__class__,
                                        request=request, user=user)
                    return Response(user_details, status=status.HTTP_200_OK)

                except Exception as error:
                    print(traceback.format_exc())
                    raise error
            else:
                res = {'error': 'can not authenticate with the given '
                                'credentials or the account has been deactivated'}
                return Response(res, status=status.HTTP_403_FORBIDDEN)
        except KeyError:
            res = {'error': 'please provide a email and a password'}
            return Response(res, status=status.HTTP_401_UNAUTHORIZED)
