from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Profile


class UserSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email',
                  'password')
        extra_kwargs = {'password': {'write_only': True}}


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta(object):
        model = Profile
        fields = ('id', 'user', 'bio', 'date_of_birth')

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user = User.objects.create_user(**user_data)
        profile = Profile.objects.get(user=user)
        profile.bio = validated_data.get('bio')
        profile.date_of_birth = validated_data.get('date_of_birth')
        profile.save()
        return profile
