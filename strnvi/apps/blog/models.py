from django.db import models

from strnvi.apps.profiles.models import Profile

# TODO: clean migrations


class Post(models.Model):
    author = models.ForeignKey(Profile,
                               related_name='post',
                               on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('title', '-created')

    def __repr__(self):
        return '{} | {}'.format(self.title, self.author.user.get_full_name())


class Likes(models.Model):
    post = models.ForeignKey(Post,
                             related_name='likes',
                             on_delete=models.CASCADE)
    likes = models.IntegerField(null=True, default=0)
    dislikes = models.IntegerField(null=True, default=0)

    def like(self):
        self.likes += 1
        self.save()

    def unlike(self):
        self.dislikes += 1
        self.save()
