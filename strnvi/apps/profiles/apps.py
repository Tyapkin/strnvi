from django.apps import AppConfig


class ProfilesConfig(AppConfig):
    name = 'strnvi.apps.profiles'

    def ready(self):
        import strnvi.apps.profiles.signals
