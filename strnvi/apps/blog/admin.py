from django.contrib import admin

from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author_full_name', 'created']
    search_fields = ['title', 'author', 'body']
    list_filter = ['author', 'created', 'updated']

    def author_full_name(self, obj):
        return obj.author.user.get_full_name()

    author_full_name.short_description = 'Name'


admin.site.register(Post, PostAdmin)
