from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly

from .serializers import PostSerializer
from .models import Post, Likes


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user.profile)

    def perform_update(self, serializer):
        if serializer.instance.author != self.request.user.profile:
            raise PermissionDenied()
        serializer.save()

    def perform_destroy(self, instance):
        if instance.author != self.request.user.profile:
            raise PermissionDenied()
        instance.delete()


class LikesViewSet(viewsets.ViewSet):
    permission_classes = (AllowAny,)
    http_method_names = ['put']

    def like_dislike(self, request, pk):
        likes_obj = Likes.objects.update_or_create(post_id=pk)[0]
        action = request.data['action']

        if action == 'up':
            likes_obj.like()
        elif action == 'down':
            likes_obj.unlike()

        return Response(status=status.HTTP_200_OK)
