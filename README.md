# strnvi

simple rest api

Object of this task is to create a simple REST API. You have to use Django and Django rest
framework.

1. Clone repository
2. Install all packages via "pip install -r requirements.txt"
3. Run server "./manage.py runserver"
4. Start autobot "./manage.py bot"

# Enjoy.
