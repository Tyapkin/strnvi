from django.urls import path

from strnvi.apps.blog import views

app_name = 'blog'

posts = views.PostViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

post_detail = views.PostViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

post_action = views.LikesViewSet.as_view({
    'put': 'like_dislike'
})

urlpatterns = [
    path('posts/', posts, name='posts'),
    path('posts/<int:pk>/', post_detail, name='post_detail'),
    path('posts/<int:pk>/action/', post_action, name='post_action')
]
