import string
import random
import requests
from datetime import datetime
from faker import Faker

from django.core.management.base import BaseCommand
from django.conf import settings


class InitUsers(object):
    """
    generating users data
    """
    def init_chars(self, start, stop):
        random_chars = ''.join(random.sample(string.ascii_letters,
                                             random.randint(start, stop)))
        return random_chars

    def init_email(self):
        return '{}@{}'.format(self.init_chars(4, 12), settings.EMAIL_DOMAIN)

    def init_username(self):
        return self.init_chars(4, 8)

    def _init_password(self):
        return self.init_chars(8, 12)

    def generate_post_body_text(self):
        return ''.join([random.choice(string.printable) for _ in range(255)])

    def init_birthday(self):
        from_date = datetime(1960, 1, 1).date()
        to_date = datetime(2010, 12, 31).date()
        return str(Faker().date_between(start_date=from_date, end_date=to_date))


class Command(BaseCommand):
    help = 'starts autobot'

    def __init__(self, *args, **kwargs):
        self.num_of_users = settings.NUMBER_OF_USERS
        self.max_posts_per_user = settings.MAX_POSTS_PER_USER
        self.max_likes_per_user = settings.MAX_LIKES_PER_USER
        super().__init__(*args, **kwargs)

    def sign_up(self):
        path = settings.PROFILES_URL
        _password = InitUsers()._init_password()
        data = {
            "user": {
                "username": InitUsers().init_username(),
                "first_name": InitUsers().init_username(),
                "last_name": InitUsers().init_username(),
                "email": InitUsers().init_email(),
                "password": _password
            },
            "bio": InitUsers().init_chars(25, 50),
            "date_of_birth": InitUsers().init_birthday()
        }
        resp = requests.post(url=path, json=data)
        username = resp.json().get('user').get('username')
        author_id = resp.json().get('user').get('id')
        return username, author_id, _password

    def log_in(self, username, password):
        path = settings.PROFILES_URL + 'log_in/'
        data = {
            'username': username,
            'password': password
        }
        resp = requests.post(url=path, json=data)
        _jwt = resp.json().get('token')
        return _jwt

    def log_out(self):
        path = settings.PROFILES_URL + 'api-auth/logout/'
        resp = requests.get(url=path)
        return resp.json()

    def create_post(self, author_id, token):
        path = settings.URL + 'posts/'
        data = {
            'title': InitUsers().init_chars(10, 30),
            'body': InitUsers().generate_post_body_text()
        }
        headers = {
            'Authorization': 'JWT {}'.format(token)
        }
        resp = requests.post(url=path, json=data, headers=headers)
        return resp.json().get('id')

    def post_action(self, post_id):
        path = settings.URL + 'posts/{}/action/'.format(post_id)
        data = {
            'action': random.choice(['up', 'down'])
        }
        resp = requests.put(url=path, json=data)
        return resp.status_code

    def handle(self, *args, **options):
        start = datetime.now()
        posts = []
        for i in range(0, self.num_of_users + 1):
            print('========== create user ==========')
            username, author_id, password = self.sign_up()
            print('========== user {} created =========='.format(username))
            print('========== action {} =========='.format(username))
            jwt_token = self.log_in(username=username, password=password)
            print('========== user {} logged in =========='.format(username))
            num_of_posts = random.randint(1, self.max_posts_per_user + 1)
            num_of_likes = random.randint(1, self.max_likes_per_user + 1)
            print('========== user creates posts ==========')
            while num_of_posts != 0:
                post_id = self.create_post(author_id=author_id, token=jwt_token)
                posts.append(post_id)
                num_of_posts -= 1
            print('========== user likes posts ==========')
            while num_of_likes != 0:
                post = random.choice(posts)
                self.post_action(post_id=post)
                num_of_likes -= 1
            self.log_out()
            print('========== user logged out ==========')
        end = (datetime.now() - start).microseconds
        print('========== done {} =========='.format(end))

