from rest_framework import serializers

from .models import Post, Likes


class LikesSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = Likes
        fields = ['likes', 'dislikes']


class PostSerializer(serializers.ModelSerializer):
    likes = LikesSerializer(many=True, read_only=True)

    class Meta(object):
        model = Post
        fields = ['id', 'author', 'title', 'body', 'created', 'updated', 'likes']
        extra_kwargs = {
            'author': {'read_only': True},
        }
