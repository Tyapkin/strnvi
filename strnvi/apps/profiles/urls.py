from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ProfileViewSet

app_name = 'profiles'
router = DefaultRouter()
router.register(r'profiles', ProfileViewSet, basename='profiles')

urlpatterns = [
    path('', include(router.urls)),
    path('profiles/api-auth/', include('rest_framework.urls',
                                       namespace='rest_framework'))
]
